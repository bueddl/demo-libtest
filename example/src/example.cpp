#include <memory>

#include <abc/abc.h>

int main()
{
  auto o = std::make_shared<abc::a>(42);

  o->foo();
}
