#include <iostream>

#include <abc.h>

namespace abc
{

a::a(int v) noexcept
  : v_{v}
{}

void a::foo()
{
  std::cout << "a::a::foo() v=" << v_ << "\n";
}

}
