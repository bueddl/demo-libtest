#pragma once

namespace abc
{

class a
{
public:
  explicit a(int v) noexcept;

  void foo();

private:
  int v_;
};

}
